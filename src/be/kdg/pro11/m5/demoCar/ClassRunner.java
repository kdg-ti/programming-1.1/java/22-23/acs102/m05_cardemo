package be.kdg.pro11.m5.demoCar;

public class ClassRunner {
  public static void main(String[] args) {
    Car myCar = new Car();
    myCar.pressGas();
    myCar.setBrand("Volkswagen");
    double newSpeed=myCar.pressGas(20);
    System.out.printf("Car with brand %s drives at %.0f km/h\n",
        myCar.getBrand(),newSpeed);
    Car yourCar=new Car();
   // Color redPaint = new Color();
   //  redPaint.red=255;
    Color redPaint = new Color(255,0,0);
    FabiansColour grey = new FabiansColour(127, 127, 127);

    Car badCar = new Car();
   // speed is private!
    // badCar.speed=-100.0;
    //redPaint.blue=-1000;


    yourCar.setColor( redPaint);
    System.out.printf("Car with brand %s drives at %.0f km/h" ,
        yourCar.getBrand(),yourCar.getSpeed());
    System.out.println(" with color " + redPaint);
  }
}
