package be.kdg.pro11.m5.demoCar;

public class Color {
  private int red=0;
  private int green=0;
  private int blue=0;

// public Color(int red,int green,int blue){
//   if (red >= 0 && red < 255 && green >= 0 && green < 255 && blue >= 0 && blue < 255) {
//     this.red = red;
//     this.green = green;
//     this.blue = blue;
//   }
//  }

  public Color(int red,int green,int blue){
    if (isColorValid(red) && isColorValid(green) && isColorValid(blue)) {
      this.red = red;
      this.green = green;
      this.blue = blue;
    }
  }

  private boolean isColorValid(int rgb){
    return rgb >= 0 && rgb <= 255;
  }

 public Color(){
    this(127, 127, 127);
  }

public void setRed(int red){
   this.red=red;
}



  public String toString(){
    return String.format("red(%d),green(%d),blue(%d)", red, green, blue);
  }
}