package be.kdg.pro11.m5.demoCar;

public class Car {
  private double speed=100.0;
  private String brand;
  private Color color;

  public String getBrand() {
    return brand;
  }


  public void setBrand(String brand){
    this.brand = brand;
  }

  public Color getColor() {
    return color;
  }

  public void setColor(Color color) {
    this.color = color;
  }

  @Override
  public String toString() {
    return "Car{" +
        "speed=" + speed +
        ", brand='" + brand + '\'' +
        ", color=" + color +
        '}';
  }


  // the default constructor
  public Car(){
    speed=0.0;
    brand="Tesla";
    color = new Color();
  }



  public double getSpeed(){
    return speed;
  }

  public  double pressGas(){
//    speed += 10;
//    return speed;
    return pressGas(1);
  }

  public double pressGas(int force){
    if (force < 1 ){
      return speed;
    } else if(force > 5){
      force = 5;
    }
    speed += 10*force;
    return speed;
  }



}
