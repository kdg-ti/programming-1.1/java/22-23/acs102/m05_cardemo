package be.kdg.pro11.m5.ex01_book;

public class Book {
  private String author;
  private String title;
  private int pages;
  private boolean onLoan;

  public Book(String author, String title, int pages) {
    this.author = author;
    this.title = title;
    this.pages = pages;
  }

  public Book(){
    this("unknown", "unknown", 0);
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getPages() {
    return pages;
  }

  public void setPages(int pages) {
    this.pages = pages;
  }

  public boolean isOnLoan() {
    return onLoan;
  }

  public void setOnLoan(boolean onLoan) {
    this.onLoan = onLoan;
  }

  // Book JAVA, HOW TO PROGRAM (1186 pages), written by DEITEL & DEITEL is  on loan.

  @Override
  public String toString() {
//    return "Book " +
//         title +
//        " (" +pages +")"+
//        ", written by " + author +
//        " is " + (onLoan?"on loan.":"available.")
//        ;
    return String.format("Book %s (%d), written by %s is %s" ,
        title ,
        pages ,
        author ,
        (onLoan?"on loan.":"available.")
    );
  }
}
