package be.kdg.pro11.m5.ex01_book;

public class TestBook {
  public static void main(String[] args) {
    Book unknown = new Book();
    System.out.println(unknown);
    Book silmarillion = new Book("J.R.R Tolkien","The Silmarillion",842);
    silmarillion.setOnLoan(true);
    System.out.println(silmarillion);

  }
}
